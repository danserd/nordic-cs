﻿using System;

namespace Colors
{
    class Program
    {
        [Flags]
        enum Colors
        {
            Black = 0x1,
            Blue = 0x1 << 1,
            Cyan = 0x2 << 2,
            Grey = 0x1 << 3,
            Green = 0x1 << 4,
            Magenta = 0x1 << 5,
            Red = 0x1 << 6,
            White = 0x1 << 7,
            Yellow = 0x1 << 8
        }
        static void Main(string[] args)
        {
            Colors myColors = 0;
            foreach (Colors colors in Enum.GetValues(typeof(Colors)))
            {
                myColors |= colors;
            }

            Console.WriteLine(myColors);

            Colors favoriteColors = 0;
            for (int i = 0; i < 4; i++)
            {
                Console.Write($"Введите 4 любимых цвета {i + 1} :");

                var color = Enum.Parse(typeof(Colors), Console.ReadLine());
                favoriteColors |= (Colors)color;
            }

            Console.WriteLine();
            Console.Write($"Любимые цвета: {favoriteColors}");

            Console.WriteLine();
            Colors badColors = myColors ^ favoriteColors;
            Console.WriteLine($"Нелюбимые цвета: {badColors}");

            Console.ReadKey();
        }

    }
}