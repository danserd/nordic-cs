﻿using System;

namespace Juice
{
    class Program
    {
        [Flags]
        enum Liters : byte
        {
            None = 0x0,
            OneLiter = 0x1,
            FiveLiters = 0x2,
            TwentyLiters = 0x4
        };
        static void Main(string[] args)
        {
            Console.WriteLine(" Какой объем сока (в литрах) требуется упаковать? ");
            double input = double.Parse(Console.ReadLine());

            Console.WriteLine($"Вы ввели {input}");

            Liters pickUsed = Liters.OneLiter ^ Liters.FiveLiters ^ Liters.TwentyLiters;

            double oneLiter = Math.Floor(((input % 20) % 5) / 1);
            double fiveLiters = Math.Floor((input % 20) / 5);
            double twentyLiters = Math.Floor(input / 20);



            Console.WriteLine($"Вам потребуются следующие контейнеры:" +
                $"1л:{oneLiter} шт\n" +
                $"5л: {fiveLiters} шт\n" +
                $"20л:{twentyLiters} шт");
        }
    }
}
 

