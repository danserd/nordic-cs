﻿using System;

class Program
{
	static void Main(string[] args)
	{
		Console.Write("Enter integer value between 1 and 100: ");
		string inputString = Console.ReadLine();

		int numberOfYears = 20;

		try
		{
			numberOfYears = int.Parse(inputString);
		}
		catch (FormatException e)
		{
			Console.WriteLine(
				$"Произошла ошибка типа {e.GetType()}\n" +
				$"'{e.Message}'\n" +
				$"Мы используем значение по умолчанию {numberOfYears}");
		}
		catch (OverflowException e)
		{
			Console.WriteLine(
				$"Произошла ошибка типа {e.GetType()}\n" +
				$"'{e.Message}'\n" +
				$"Мы используем значение по умолчанию {numberOfYears}");
		}
		finally
		{
			numberOfYears++;
			Console.WriteLine(
				$"Мы в finally блоке, " +
				$"значение переменной numberOfYears = {numberOfYears}");
		}

		if (numberOfYears < 0 || numberOfYears > 100)
		{
			throw new ArgumentOutOfRangeException();
			Console.WriteLine($"You entered wrong value!");
			return;
		}

		if (numberOfYears == 1
			|| numberOfYears == 21
			|| numberOfYears == 31
			|| numberOfYears == 41
			|| numberOfYears == 51
			|| numberOfYears == 61
			|| numberOfYears == 71
			|| numberOfYears == 81
			|| numberOfYears == 91)
		{
			Console.WriteLine($"{numberOfYears} год");
		}
		else if (
			(numberOfYears > 1 && numberOfYears < 5)
			|| (numberOfYears > 21 && numberOfYears < 25)
			|| (numberOfYears > 31 && numberOfYears < 35)
			|| (numberOfYears > 41 && numberOfYears < 45)
			|| (numberOfYears > 51 && numberOfYears < 55)
			|| (numberOfYears > 61 && numberOfYears < 65)
			|| (numberOfYears > 71 && numberOfYears < 75)
			|| (numberOfYears > 81 && numberOfYears < 85)
			|| (numberOfYears > 91 && numberOfYears < 95))
		{
			Console.WriteLine($"{numberOfYears} года");
		}
		else
		{
			Console.WriteLine($"{numberOfYears} лет");
		}

		if (numberOfYears == 1
			|| (numberOfYears > 20
				&& inputString.EndsWith('1')))
		{
			Console.WriteLine($"{numberOfYears} год");
		}
		else if (
			numberOfYears != 12
			&& numberOfYears != 13
			&& numberOfYears != 14
			&& (inputString.EndsWith('2')
				|| inputString.EndsWith('3')
				|| inputString.EndsWith('4')))
		{
			Console.WriteLine($"{numberOfYears} года");
		}
		else
		{
			Console.WriteLine($"{numberOfYears} лет");
		}
	}
}
