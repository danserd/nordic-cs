﻿using System;

namespace L05_SW_C02_ternary_operator
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Enter integer value between 1 and 100: ");
			int number = int.Parse(Console.ReadLine());

			Console.WriteLine(number < 50
				? "Число меньше 50"
				: "Число больше либо равно 50");
		}
	}
}
