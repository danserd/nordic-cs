﻿using System;

namespace L05_SW_C03_exception_demo
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				int a = RequestIntegerFromConsole("Введите сторону прямоугольника a");
				int b = RequestIntegerFromConsole("Введите сторону прямоугольника b");
				int c = a * b;
				Console.WriteLine($"Площадь прямоугольника равна {c}");
			}
			catch (Exception e)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine("Произошла ошибка работы программы!");
				throw;
			}

			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("Программа завершилась успешно!");
		}

		static int RequestIntegerFromConsole(
			string requestMessage)
		{
			Console.WriteLine(requestMessage);
			return int.Parse(Console.ReadLine());
		}
	}
}
