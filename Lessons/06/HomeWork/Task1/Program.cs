﻿using System;

namespace Task1
{
    class Program
    {

        static void Main()
        {
            Exception();
            Console.WriteLine("Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }
        static void Exception()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите положительное натуральное число не более 2 миллиардов");
                    int a = Int32.Parse(Console.ReadLine());

                    if (a > 2000000000)
                    {
                        throw new Exception("число больше двух миллиардов");
                    }
                    if (a < 0)
                    {
                        throw new Exception("Введено неверное значение! Попробуйте ещё раз: ");
                    }

                    int b = 0;
                    while (a != 0)
                    {
                        if ((a % 10) % 2 == 0)
                        {
                            b++;
                        }
                        a = a / 10;
                    }
                    Console.WriteLine($"Количество четных цифр: {b}");
                    break;
                }

                catch (OverflowException)
                {
                    Console.WriteLine("Ошибка System.OverflowException! Попробуйте ещё раз:");
                }

                catch (FormatException)
                {
                    Console.WriteLine(" Ошибка System.FormatException! Попробуйте ещё раз:");
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}



















