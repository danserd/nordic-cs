﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            string again = "Д";
            while (again == "Д")

            {
                try
                {
                    Console.WriteLine("Введите сумму первоначального взноса в рублях: ");
                    double a = double.Parse(Console.ReadLine());
                    Console.WriteLine("Введите ежедневный процент дохода в виде десятичной дроби (1% =0,01): ");
                    double b = double.Parse(Console.ReadLine());
                    Console.WriteLine("Введите желаемую сумму накопления в рублях: ");
                    double c = double.Parse(Console.ReadLine());

                    int d = 0;
                    for (int i = 0; a < c; ++i)
                    {
                        a += (a * b);

                        d++;
                        if (a > c) break;
                    }


                    Console.WriteLine($"Необходимое количество дней для накопления желаемой суммы:{d}");
                    Console.WriteLine("Нажмите любую клавишу для выхода...");
                }
                catch (FormatException)
                {
                    Console.WriteLine(" Ошибка System.FormatException! Попробуйте ещё раз:");
                }
                Console.WriteLine("Выйти ? (Д/Н)");
                again = Console.ReadLine();

            }

        }
    }
}

