using System;

class Program
{
	static void Main()
	{
		Console.WriteLine("Enter two real numbers to multiply them:");
		var d1 = double.Parse(Console.ReadLine());
		var d2 = double.Parse(Console.ReadLine());
		Console.WriteLine(d1 + " * " + d2 + " = " + d1 * d2);
		Console.WriteLine("{0}.## + {1}.## = {2}.##", d1, d2, d1 + d2);
		Console.WriteLine($"{d1:#.##} + {d2:#.##} = {(d1 * d2):#.##}");
		Console.WriteLine($"{d1:0.##} + {d2:0.##} = {(d1 * d2):0.##}");
	}
}