﻿using System;

namespace Task2
{

    class Program
    {
        static void Main(string[] args)
        {

            string input;
            string[] word;
            int wordsStartA = 0;

            Console.WriteLine(" Введите строку из нескольких слов(как минимум одно слово должно начинаться с бувы 'А')");

            while (true)
            {

                input = Console.ReadLine();

                word = input.Split(' ');
                if (word.Length >= 2)
                    break;
                Console.WriteLine("Слишком мало слов. Попробуйте ещё раз");
            }

            foreach (string symbol in word)
            {
                if (symbol[0] == 'А' || symbol[0] == 'а')
                    wordsStartA += 1;
            }

            Console.WriteLine($"Количество слов, начинающихся с буквы 'А: {wordsStartA}");

            Console.WriteLine("Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }
    }
}





















