﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {

            string again = "нет";
            while (again == "нет")
            {
                Console.WriteLine("Введите непустую строку");
                string input;
                string empty = string.Empty;
                input = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(input))
                    {
                        throw new Exception("Пустая строка! Попробуйте ещё раз");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                for (int i = input.Length - 1; i >= 0; i--)
                {
                    empty += input[i];
                }

                Console.WriteLine(empty.ToLower());

                Console.WriteLine("Закрыть программу ? (да/нет)");
                again = Console.ReadLine();
            }
        }
    }
}





