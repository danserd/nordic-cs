﻿using System;

namespace PifagorApp
{
    class Program
    {
        static void Main()
        {
            int[] vertical =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10
            };

            int[] horizontal =
            {
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10
            };

            for (int i = 0; i < vertical.Length; i++)
            {
                for (int j = 0; j < horizontal.Length; j++)
                {
                    Console.Write($"\t{vertical[i] * horizontal[j]}");
                }
                Console.WriteLine();
            }

            //int[,] arrays = new int[10, 10];
            //for (int i = 1; i < 10; i++)
            //{
            //    for (int j = 1; j < 10; j++)
            //    {
            //        Console.Write($"\t{i * j}");
            //    }
            //    Console.WriteLine();
            //}

        }
    }
}



