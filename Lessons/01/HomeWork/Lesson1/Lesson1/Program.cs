﻿using System;
using System.Text;
using System.Threading;

class Program
{
    static void Main()
    {
        Console.InputEncoding = Encoding.Unicode;
        Console.OutputEncoding = Encoding.Unicode;

        Console.WriteLine("Введите имя");
         string name = Console.ReadLine();
        Thread.Sleep(5000);
        Console.WriteLine($"Привет {name}!");
        Thread.Sleep(5000);
        Console.WriteLine($"Пока {name}!");
        Console.WriteLine("Нажмите любую клавишу для выхода");
        Console.ReadKey();
    }
}
       