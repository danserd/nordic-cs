﻿using System;
using System.Collections.Generic;

namespace Brackets
{
    class Program
    {
        static void Main(string[] args)
        {
            var stack = new Stack<char>();

            Console.WriteLine("Введите выражение из скобок");
            string input = Console.ReadLine();

            foreach(var bracket in input)
            {
                switch (bracket)
                {
                    case '(':
                        stack.Push('(');
                        continue; 

                    case '[':
                        stack.Push('[');
                        continue; 

                    case ')':
                        if (stack.Count == 0)
                        {
                            Console.WriteLine("Ошибка");
                        }
                        else if (stack.Peek() == '(')
                        {
                            stack.Pop();
                        }
                        continue; 

                    case ']':
                        if (stack.Count == 0)
                        {
                            Console.WriteLine("Ошибка");
                        }
                        else if (stack.Peek() == '[')
                        {
                            stack.Pop();
                        }
                        continue; 
                }
            }

            if (stack.Count != 0)
            {
                Console.WriteLine("Ошибка");
            }
            else
                Console.WriteLine("Всё верно");
           
        }
    }
}
