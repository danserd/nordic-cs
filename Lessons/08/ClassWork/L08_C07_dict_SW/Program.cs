using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
	static void Main()
	{
		var countries = new Dictionary<string, string>
		{
			{"Россия", "Москва" },
			{"Франция", "Париж"},
			{"Германия", "Берлин"},
			{"Великобритания", "Лондон"}
		};

		while (true)
		{
			var index = (new Random()).Next(4);
			var kvp = countries.ElementAtOrDefault(index);
			var country = kvp.Key;
			var capital = kvp.Value;

			Console.Write($"Введите столицу страны \"{country}\": ");
			var answer = Console.ReadLine()?.Trim();
			if (answer == capital)
			{
				Console.WriteLine("Правильно!");
			}
			else
			{
				Console.WriteLine("Вы проиграли :( Выходим...");
				break;
			}
		}
		Console.ReadKey();
	}
}