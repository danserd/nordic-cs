using System;
using System.Collections.Generic;

class Program
{
	static void Main()
	{
		var queue = new Queue<int>();
 
		Console.WriteLine("Вводите числа для создания отложенных задач возведения их в квадрат.");
		Console.WriteLine("Введите \"run\" чтобы запустить расчет, или \"exit\" чтобы завершить работу.");
		string data;
		while (true)
		{
			data = Console.ReadLine().ToLower();
			if (data == "run")
			{
				while (queue.Count > 0)
				{
					var number = queue.Dequeue();
					Console.WriteLine($"{number}^2 = {number * number}");
				}
				continue;
			}
			else if (data == "exit")
			{
				Console.WriteLine($"Выходим. Число задач в очереди на момент выхода: {queue.Count}");
				break;
			}
		 
			queue.Enqueue(int.Parse(data));
		}
		Console.ReadKey();
	}
}