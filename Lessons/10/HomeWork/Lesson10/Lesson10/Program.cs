﻿using System;

namespace Lesson10
{
    class Program
    {
        public static void Main(string[] args)
        {
            var person = new Person[3];

            for (int i = 0; i < person.Length; i++)
            {
                person[i] = new Person();

                person[i].SendConsole();
            }

            for (int i = 0; i < person.Length; i++)
            {
                person[i].Description();

            }

            Console.WriteLine("Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }
    }
}



















