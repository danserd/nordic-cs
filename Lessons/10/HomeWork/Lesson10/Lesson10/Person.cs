﻿using System;

namespace Lesson10
{
    class Person
    {
        private int _age;

        public string Name;

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {

                if (value < 150 && value > 0)
                {

                    _age = value;
                }
                else
                    throw new Exception("Введен некорректный возраст");
            }
        }
        public void SendConsole()
        {
            Console.WriteLine("Введите имя");
            Name = Console.ReadLine();
            do
            {

                try
                {
                    Console.WriteLine("Введите возраст");
                    Age = int.Parse(Console.ReadLine());

                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (true);


        }
        public int AgeInYears(int numberOfYear)
        {
            return Age + numberOfYear;
        }

        public string Message(int numberOfYear)
        {
            return ($"{Name} возраст через {numberOfYear} года будет {AgeInYears(numberOfYear)}");
        }

        public void Description(int numberOfYear = 4)
        {
            Console.WriteLine(Message(numberOfYear));
        }
    }
}


























