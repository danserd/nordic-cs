﻿using System;

namespace Exceptions
{
    class Program
    {

        static void Main(string[] args)
        {
            string again = "yes";
            while (again == "yes")
            {
                ArrayFigure();
                Console.WriteLine("Do you want to continue working ? (YES/NO)");
                again = Console.ReadLine();
            }
        }

        public static void ArrayFigure()
        {
            try
            {
                var figureArray = (Figures[])Enum.GetValues(typeof(Figures));
                Console.Write("Enter type geometry figure :  ");

                for (var i = 0; i < figureArray.Length; i++)
                {
                    Console.Write($"{figureArray[i]}: ");
                }
                var input = Console.ReadLine();
                var getValue = (Figures)Enum.Parse(typeof(Figures), input);
                Console.WriteLine($"pick: {getValue}");

                switch (Convert.ToInt32(input))
                {
                    case 1:
                        Console.WriteLine(" Enter the circle diameter");
                        double r = Convert.ToDouble(Console.ReadLine());

                        MathCircle(r);
                        break;
                    case 2:
                        Console.WriteLine(" Enter the equilateral triangle length");
                        double c = Convert.ToDouble(Console.ReadLine());

                        Triangle(c);
                        break;
                    case 3:
                        Console.WriteLine(" Enter the rectangle diameter");
                        double a = Convert.ToDouble(Console.ReadLine());

                        double b = Convert.ToDouble(Console.ReadLine());

                        Rectangle(a, b);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ResetColor();

            }
        }

        static void MathCircle(double r)
        {
            var resS = Math.PI * Math.Pow(r, 2);
            var resP = 2 * Math.PI * r;

            Console.WriteLine($"Square circle = {resS}, circle perimeter length = {resP}");
        }

        static void Triangle(double c)
        {
            var resS = 0.5 * c * c;
            var resP = 3 * c;
            Console.WriteLine($"Square equilateral triangle = {resS}, triangle perimeter length = {resP}");
        }
        static void Rectangle(double a, double b)
        {
            var resS = a * b;
            var resP = (a + b) * 2;
            Console.WriteLine($"Square Rectangle = {resS}, rectangle perimeter length = {resP}");
        }
    }
}
















