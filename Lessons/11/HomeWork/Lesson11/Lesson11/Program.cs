﻿using System;
using System.Collections.Generic;

namespace Lesson11
{
    class Program
    {
        static void Main(string[] args)
        {
            var reminderItems = new ReminderItem[]
            {
                new ReminderItem(
                    DateTimeOffset.Parse("17-09-2019"),
                    "Good morning"
                    ),
                new PhoneReminderItem(
                    DateTimeOffset.Parse("18-09-2019"),
                    "Hi",
                    "8-800-955"
                    ),
                new ChatReminderItem(
                    DateTimeOffset.Parse("18-09-2019"),
                    "Hi",
                    "Super Chat",
                    "Login_NoName"
                    )
            };

            foreach(var send in reminderItems)
            {
                send.WriteProperties();
            }

            Console.ReadKey();
        }
    }
}



