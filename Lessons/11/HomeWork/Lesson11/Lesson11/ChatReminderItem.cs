﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class ChatReminderItem : ReminderItem
    {
        public string ChatName { get; }

        public string AccountName { get; set; }

        public ChatReminderItem(DateTimeOffset alarmDate, string alarmMessage, 
            string chatName, string accountName)
            : base(alarmDate, alarmMessage)
        {
            ChatName = chatName;
            AccountName = accountName;
        }

        public override void WriteProperties()
        {
            Console.WriteLine();
            base.WriteProperties();
            Console.Write($"Название чата: {ChatName}\n");
            Console.Write($"Логин: {AccountName}\n");
        }
    }
}
