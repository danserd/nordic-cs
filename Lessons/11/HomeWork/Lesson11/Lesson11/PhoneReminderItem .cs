﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class PhoneReminderItem : ReminderItem
    {
        public string PhoneNumber { get; set; }

        public PhoneReminderItem(DateTimeOffset alarmDate, string alarmMessage, string phoneNumber)
            : base(alarmDate, alarmMessage)
        {
            PhoneNumber = phoneNumber;
        }

        public override void WriteProperties()
        {
            Console.WriteLine();
            base.WriteProperties();
            Console.Write($"Номер телефона: {PhoneNumber}\n");
        }
    }
}



