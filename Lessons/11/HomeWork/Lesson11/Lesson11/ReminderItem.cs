﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class ReminderItem
    {
        public DateTimeOffset AlarmDate { get; set; }
        public string AlarmMessage { get; }
        public TimeSpan TimeToAlarm
        {
            get
            {
                { return AlarmDate - DateTime.Now; }
            }
        }

        public bool IsOutdated
        {
            get
            {
               
                {
                    return TimeToAlarm >= TimeSpan.Zero;
                }
            }
        }

        public  ReminderItem(DateTimeOffset alarmDate, string alarmMessage)
        {
            AlarmDate = alarmDate;
            AlarmMessage = alarmMessage;
        }

        public virtual void WriteProperties()
        {
            Console.WriteLine($"Тип объекта: { GetType()}\n"+
                               $"Дата будильника: {AlarmDate}\n" +
                               $"Сообщение: {AlarmMessage}\n" +
                               $"Текущее время: {TimeToAlarm}\n" +
                               $"Просрочено ли событие: {IsOutdated}");
        }

    }
}
